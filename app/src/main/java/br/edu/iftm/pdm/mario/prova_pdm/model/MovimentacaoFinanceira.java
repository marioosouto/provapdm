package br.edu.iftm.pdm.mario.prova_pdm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MovimentacaoFinanceira implements Parcelable {

    private String tipo_movimentacao;
    private String titulo_movimentacao;
    private float valor;

    public MovimentacaoFinanceira(String tipo_movimentacao, String titulo_movimentacao, float valor) {
        this.tipo_movimentacao = tipo_movimentacao;
        this.titulo_movimentacao = titulo_movimentacao;
        this.valor = valor;
    }

    public String getTipo_movimentacao() {
        return tipo_movimentacao;
    }

    public void setTipo_movimentacao(String tipo_movimentacao) {
        this.tipo_movimentacao = tipo_movimentacao;
    }

    public String getTitulo_movimentacao() {
        return titulo_movimentacao;
    }

    public void setTitulo_movimentacao(String titulo_movimentacao) {
        this.titulo_movimentacao = titulo_movimentacao;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    protected MovimentacaoFinanceira(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovimentacaoFinanceira> CREATOR = new Creator<MovimentacaoFinanceira>() {
        @Override
        public MovimentacaoFinanceira createFromParcel(Parcel in) {
            return new MovimentacaoFinanceira(in);
        }

        @Override
        public MovimentacaoFinanceira[] newArray(int size) {
            return new MovimentacaoFinanceira[size];
        }
    };
}
