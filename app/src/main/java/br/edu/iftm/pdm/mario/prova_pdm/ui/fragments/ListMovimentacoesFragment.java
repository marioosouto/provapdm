package br.edu.iftm.pdm.mario.prova_pdm.ui.fragments;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import br.edu.iftm.pdm.mario.prova_pdm.R;
import br.edu.iftm.pdm.mario.prova_pdm.data.DAOMovimentacaoFinanSingleton;
import br.edu.iftm.pdm.mario.prova_pdm.ui.activities.MainActivity;
import br.edu.iftm.pdm.mario.prova_pdm.ui.list_builders.MovimentacoesListBuilder;

public class ListMovimentacoesFragment extends Fragment
                                implements View.OnClickListener {

    private FloatingActionButton fabNewArt;
    private MovimentacoesListBuilder artsListBuilder;

    public ListMovimentacoesFragment() {
        super(R.layout.activity_lista_movimentacoes);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.fabNewArt.setOnClickListener(this);
        this.artsListBuilder = new MovimentacoesListBuilder(view, R.id.rvMovimentacao)
                                    .load(DAOMovimentacaoFinanSingleton.getINSTANCE().getMovimentacoes(view.getContext()));
    }

    @Override
    public void onClick(View v) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(MainActivity.MAIN_BACK_STACK)
                .replace(R.id.fcvManager, NewMovimentacaoFragment.class, null)
                .commit();
    }
}
