package br.edu.iftm.pdm.mario.prova_pdm.ui.list_builders;

import android.app.Activity;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

import br.edu.iftm.pdm.mario.prova_pdm.model.MovimentacaoFinanceira;
import br.edu.iftm.pdm.mario.prova_pdm.ui.lists.movimentacoes.MovimentacaoDataAdapter;


public class MovimentacoesListBuilder implements MovimentacaoDataAdapter.OnClickMovimentacaoListener{

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private MovimentacaoDataAdapter adapter;
    private ArrayList<String> arts;
    private OnClickCaptureButtonListener listener;

    @Override
    public void onclickMovimentacao(MovimentacaoFinanceira movimentacao) {
        if(movimentacao == null) {
            if(this.listener != null){
                this.listener.onClickCaptureButton();
            }

        }
    }

    public interface OnClickCaptureButtonListener {
        void onClickCaptureButton();
    }

    public MovimentacoesListBuilder(View view, @IdRes int idRv) {
        this.recyclerView = view.findViewById(idRv);
        this.layoutManager = new LinearLayoutManager(view.getContext());
        this.recyclerView.setLayoutManager(this.layoutManager);
    }

    public MovimentacoesListBuilder load(ArrayList<String> arts) {
        this.arts = arts;
        this.adapter = new MovimentacaoDataAdapter(this.arts);
        this.recyclerView.setAdapter(this.adapter);
        this.adapter.setOnClickMovimentacaoListener(this);
        return this;
    }


        public MovimentacoesListBuilder setOnClickCaptureButtonListener(OnClickCaptureButtonListener listener) {
        this.listener = listener;
        return this;
    }

    public OnClickCaptureButtonListener getOnClickCaptureButtonListener() {
        return this.listener;
    }




}
