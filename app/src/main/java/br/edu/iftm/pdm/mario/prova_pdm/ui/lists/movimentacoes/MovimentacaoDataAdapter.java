package br.edu.iftm.pdm.mario.prova_pdm.ui.lists.movimentacoes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import br.edu.iftm.pdm.mario.prova_pdm.R;
import br.edu.iftm.pdm.mario.prova_pdm.model.MovimentacaoFinanceira;

public class MovimentacaoDataAdapter extends RecyclerView.Adapter<MovimentacaoDataViewHolder> {

    private ArrayList<MovimentacaoFinanceira> movimentacoes;
    private OnClickMovimentacaoListener listener;

    public interface OnClickMovimentacaoListener {
        void onclickMovimentacao(MovimentacaoFinanceira movimentacao);
    }

    public MovimentacaoDataAdapter(ArrayList<String> movimentacoes) {
        this.movimentacoes = movimentacoes;
        this.listener = null;
    }

    @NonNull
    @Override
    public MovimentacaoDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_view_movimentacao, parent, false);
        return new MovimentacaoDataViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull MovimentacaoDataViewHolder holder, int position) {
        holder.bind(this.movimentacoes.get(position));
    }

    @Override
    public int getItemCount() {
        return this.movimentacoes.size();
    }


    public void setOnClickMovimentacaoListener(OnClickMovimentacaoListener listener) {
        this.listener = listener;
    }

    public OnClickMovimentacaoListener getOnClickMovimentacaoListener(){
        return this.listener;
    }

    public void refreshMovimentacaoList() {
        int pos = this.movimentacoes.size() - 1;
        notifyItemInserted(pos);
    }

}