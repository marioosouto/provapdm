package br.edu.iftm.pdm.mario.prova_pdm.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;

import br.edu.iftm.pdm.mario.prova_pdm.R;
import br.edu.iftm.pdm.mario.prova_pdm.data.DAOMovimentacaoFinanSingleton;
import br.edu.iftm.pdm.mario.prova_pdm.model.MovimentacaoFinanceira;
import br.edu.iftm.pdm.mario.prova_pdm.ui.list_builders.MovimentacoesListBuilder;

public class NewMovimentacaoFragment extends Fragment
                 implements View.OnClickListener{

    private EditText tipo_movimentacao;
    private EditText descricao;
    private EditText valor;
    private Button btnSave;
    private ArrayList<String> picPaths;
    private MovimentacoesListBuilder listBuilder;
    ActivityResultLauncher<Intent> resultLauncher;

    public NewMovimentacaoFragment() {
        super(R.layout.activity_new_movimentacao);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if(result.getResultCode() == Activity.RESULT_OK){

                        }

                    }
                }
        );
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tipo_movimentacao = view.findViewById(R.id.SprTipoMovimentacao);
        this.descricao = view.findViewById(R.id.etxtDescricaoMovimentacao);
        this.valor = view.findViewById(R.id.txtValorMovimentacao);
        this.btnSave = view.findViewById(R.id.btnSave);
        this.btnSave.setOnClickListener(this);
        this.picPaths = new ArrayList<>();
        this.listBuilder = new MovimentacoesListBuilder(view, R.id.rvMovimentacao)
                                .load(this.picPaths)
                                .setOnClickCaptureButtonListener((MovimentacoesListBuilder.OnClickCaptureButtonListener) this);
    }

    @Override
    public void onClick(View v) {
        String name = this.tipo_movimentacao.getText().toString();
        String description = this.descricao.getText().toString();
        String priceStr = this.valor.getText().toString();

        if (name.isEmpty() || description.isEmpty() || priceStr.isEmpty())
            return;

        MovimentacaoFinanceira m = new MovimentacaoFinanceira(name, description, Float.parseFloat(priceStr));
        DAOMovimentacaoFinanSingleton.getINSTANCE().addMovimentacao(m);
        this.getActivity().onBackPressed();
    }


}
