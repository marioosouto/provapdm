package br.edu.iftm.pdm.mario.prova_pdm.data;

import android.content.Context;

import java.util.ArrayList;
import br.edu.iftm.pdm.mario.prova_pdm.model.MovimentacaoFinanceira;


public class DAOMovimentacaoFinanSingleton {

    private static DAOMovimentacaoFinanSingleton INSTANCE;
    private ArrayList<MovimentacaoFinanceira> movimentacoes;


    private DAOMovimentacaoFinanSingleton() {
        this.movimentacoes = new ArrayList<>();
    }

    public static DAOMovimentacaoFinanSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOMovimentacaoFinanSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<MovimentacaoFinanceira> getMovimentacoes(String context) {
        return this.movimentacoes;
    }

    public void addMovimentacao(MovimentacaoFinanceira movimentacao) {
        this.movimentacoes.add(movimentacao);

    }


    public ArrayList<MovimentacaoFinanceira> getArts(Context context) {
        return this.movimentacoes;
    }
}