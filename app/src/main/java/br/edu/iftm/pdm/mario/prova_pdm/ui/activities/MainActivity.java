package br.edu.iftm.pdm.mario.prova_pdm.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import br.edu.iftm.pdm.mario.prova_pdm.R;
import br.edu.iftm.pdm.mario.prova_pdm.ui.fragments.ListMovimentacoesFragment;


public class MainActivity extends AppCompatActivity {


    public static final String MAIN_BACK_STACK = "MainActivity.MAIN_BACK_STACK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fcvManager, ListMovimentacoesFragment.class, null)
                .commit();
    }


}
