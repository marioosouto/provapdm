package br.edu.iftm.pdm.mario.prova_pdm.ui.lists.movimentacoes;

import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import br.edu.iftm.pdm.mario.prova_pdm.R;
import br.edu.iftm.pdm.mario.prova_pdm.model.MovimentacaoFinanceira;


public class MovimentacaoDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private MovimentacaoFinanceira currentMovimentacao;
    private Spinner tipoTransacao;
    private TextView txtTituloMovimentacao;
    private TextView txtValor;
    private MovimentacaoDataAdapter adapter;



    public MovimentacaoDataViewHolder(@NonNull View itemView, MovimentacaoDataAdapter adapter) {
        super(itemView);
        this.tipoTransacao = itemView.findViewById(R.id.SprTipoMovimentacao);
        this.txtTituloMovimentacao = itemView.findViewById(R.id.txtDescricaoMovimentacao);
        this.txtValor = itemView.findViewById(R.id.txtValorMovimentacao);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
    }

    public MovimentacaoDataViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void bind(MovimentacaoFinanceira movimentacao){
        this.tipoTransacao = itemView.findViewById(Integer.parseInt(movimentacao.getTipo_movimentacao()));
        this.txtTituloMovimentacao.setText(String.valueOf(movimentacao.getTitulo_movimentacao()));
        this.txtValor.setText(Float.toString(movimentacao.getValor()));
        this.currentMovimentacao = movimentacao;
    }


    @Override
    public void onClick(View v) {
        if (this.adapter.getOnClickMovimentacaoListener() != null){
                this.adapter.getOnClickMovimentacaoListener().onclickMovimentacao(this.currentMovimentacao);
        }
    }
}
